﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Vidly.Models
{
    public class Customer
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        
        public bool IsSubscribedToNewsletter { get; set; }

        // This is called a navigation property because it allows us to navigate from our customer to the MembershipType:
        public MembershipType MembershipType { get; set; }

        // Sometimes for optimization we may not need to load the entire MembershipType but in some cases we just need the ID so add this:
        [Display(Name = "Membership Type")]
        public byte MembershipTypeId { get; set; }

        public DateTime? BirthDate;
    }
}